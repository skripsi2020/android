package com.example.monline24.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class UserResponse {

    @SerializedName("timestamp")
    private Date timestamp;

    @SerializedName("code")
    private String code;

    @SerializedName("object")
    private Object object;

    @SerializedName("description")
    private String description;

    @SerializedName("message")
    private String message;

    public UserResponse(Date timestamp, String code, Object object, String description, String message) {
        this.timestamp = timestamp;
        this.code = code;
        this.object = object;
        this.description = description;
        this.message = message;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
