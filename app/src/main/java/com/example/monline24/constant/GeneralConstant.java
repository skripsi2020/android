package com.example.monline24.constant;

public class GeneralConstant {

    public static final String CORE_URL = "http://192.168.1.10:8080/core";
    public static final String AUTH_URL = "http://192.168.1.10:8080/auth";
    public static final String GRANT_TYPE = "password";
    public static final String CLIENT_SECRET = "admin";
    public static final String CLIENT_ID = "admin";
    public static final String EMAIL_PATTERN = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    public static final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{4,}$";
}
