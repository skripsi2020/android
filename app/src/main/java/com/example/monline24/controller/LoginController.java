package com.example.monline24.controller;

import com.example.monline24.model.response.LoginResponse;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;


public interface LoginController {

    @FormUrlEncoded
    @POST("/oauth/token")
    void oauthToken(@Field("username") String username,
                    @Field("password") String password,
                    @Field("grant_type") String grant_type,
                    @Field("client_secret") String client_secret,
                    @Field("client_id") String client_id,
                    Callback<LoginResponse> callback);
}
