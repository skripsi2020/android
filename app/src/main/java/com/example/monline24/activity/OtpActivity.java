package com.example.monline24.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.example.monline24.R;

//Halaman cadangan kalau dibutuhin

public class OtpActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);

        //Call Function Tombol Konfirmasi OTP
        configureBtnKonfirmasiOTP();

        //Call Function Panah kembali ke Login
        configurearrowtoregister();
    }

    //Fungsi Tombol Konfirmasi di Page OTP
    private void configureBtnKonfirmasiOTP() {
        Button btnKonfirmasiOTP = (Button) findViewById(R.id.btnKonfirmasiKodeOTP);
        btnKonfirmasiOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(OtpActivity.this, MainActivity.class));
            }
        });
    }

    //Fungsi Panah Untuk Kembali Ke Register
    private void configurearrowtoregister() {
        ImageView arrowLogin = (ImageView) findViewById(R.id.arrowtoregister);
        arrowLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(OtpActivity.this, RegisterActivity.class));
            }
        });
    }
}
