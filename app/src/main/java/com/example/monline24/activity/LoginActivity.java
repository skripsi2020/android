package com.example.monline24.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.monline24.R;
import com.example.monline24.constant.GeneralConstant;
import com.example.monline24.model.response.LoginResponse;
import com.example.monline24.oauth.RestClient;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Objects;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class LoginActivity extends AppCompatActivity {

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //BUTTON FORGOT PASSWORD
        TextView textForgotPass = findViewById(R.id.btnForgetPass);
        SpannableString spannableForgotPass = new SpannableString("Lupa Password?");
        ClickableSpan clickableSpanForgetPass = new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                startActivity(new Intent(LoginActivity.this, ForgetPassActivity.class));
            }

            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(Color.WHITE);
                ds.setUnderlineText(false);
            }
        };
        spannableForgotPass.setSpan(clickableSpanForgetPass, 0, 14, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        textForgotPass.setText(spannableForgotPass);
        textForgotPass.setMovementMethod(LinkMovementMethod.getInstance());

        //BUTTON REGISTER
        TextView textRegister = findViewById(R.id.btnRegister);
        SpannableString spannableRegister = new SpannableString("Daftar");
        ClickableSpan clickableSpanRegister = new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
            }

            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(Color.WHITE);
                ds.setUnderlineText(false);
            }
        };
        spannableRegister.setSpan(clickableSpanRegister, 0, 6, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        textRegister.setText(spannableRegister);
        textRegister.setMovementMethod(LinkMovementMethod.getInstance());

        //BUTTON LOGIN
        Button buttonLogin = findViewById(R.id.btnMasuk);
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateEmail() && validatePassword()) {
                    Toast.makeText(getApplicationContext(), "Successfully Login", Toast.LENGTH_LONG).show();
                    generateTokenOauth();
                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                }else {
                    Toast.makeText(getApplicationContext(), "Failed Login", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @SuppressLint("WrongViewCast")
    private boolean validateEmail(){
        boolean valid = true;
        TextInputLayout textInputEmail = findViewById(R.id.text_input_email);
        String email = Objects.requireNonNull(textInputEmail.getEditText()).getText().toString().trim();

        if(email.isEmpty()) {
            textInputEmail.setError("Please Insert a Valid Email");
            valid = false;
        }else if (!email.matches(GeneralConstant.EMAIL_PATTERN)){
            textInputEmail.setError("Invalid Email");
            valid = false;
        }
        return valid;
    }

    @SuppressLint("WrongViewCast")
    private boolean validatePassword(){
        boolean result = true;
        TextInputLayout textInputPassword = findViewById(R.id.text_input_password);
        String password = Objects.requireNonNull(textInputPassword.getEditText()).getText().toString().trim();

        if(password.isEmpty()){
            textInputPassword.setError("Please Insert a Valid Password");
            result = false;
        } else if (!password.matches(GeneralConstant.PASSWORD_PATTERN)){
            textInputPassword.setError("Invalid Password");
            result = false;
        }
        return result;
    }

    @SuppressLint("WrongViewCast")
    private void generateTokenOauth(){
        TextInputLayout textInputEmail = findViewById(R.id.text_input_email);
        TextInputLayout textInputPassword = findViewById(R.id.text_input_password);
        String username = Objects.requireNonNull(textInputEmail.getEditText()).getText().toString();
        String password = Objects.requireNonNull(textInputPassword.getEditText()).getText().toString();

        RestClient.getLogin().oauthToken(username, password,
                GeneralConstant.GRANT_TYPE,
                GeneralConstant.CLIENT_SECRET,
                GeneralConstant.CLIENT_ID, new Callback<LoginResponse>() {

                    @Override
                    public void success(LoginResponse responseLogin, Response response) {
                        Toast.makeText(getApplicationContext(), responseLogin.getAccess_token(), Toast.LENGTH_LONG).show();

//                        Snackbar.make(linearLayout, responseLogin.getAccess_token(), Snackbar.LENGTH_LONG).show();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();

//                        Snackbar.make(linearLayout, Objects.requireNonNull(error.getMessage()), Snackbar.LENGTH_LONG).show();
                    }
                });
    }
}

