package com.example.monline24.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.monline24.R;

public class RegisterActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        //        Login Button Function
        TextView textView3 = findViewById(R.id.btnLogin);
        final String text3 = "Sudah Memiliki Akun? Masuk";
        SpannableString ss3 = new SpannableString(text3);
        ClickableSpan clickableSpanRegister = new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
            }

            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(Color.WHITE);
                ds.setFakeBoldText(true);
                ds.setUnderlineText(false);
            }
        };
        ss3.setSpan(clickableSpanRegister, 21, 26, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        textView3.setText(ss3);
        textView3.setMovementMethod(LinkMovementMethod.getInstance());


//        Call Function Button Register
        configurebtnRegister();
//        Call Function Arrow to Login
        configurearrowtologin();

    }

    //    Register Button Function
    private void configurebtnRegister() {
        final CheckBox checkBox = (CheckBox) findViewById(R.id.checkboxSyarat);
        Button btnMasuk = (Button) findViewById(R.id.btnRegister);
        btnMasuk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Checkbox Condition
                if (checkBox.isChecked()) {
                    startActivity(new Intent(RegisterActivity.this, OtpActivity.class));
                } else {
                    onStop();
                    Toast.makeText(getApplicationContext(), "Silahkan Centang Syarat dan Ketentuan Monline24", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void configurearrowtologin() {
        ImageView arrowLogin = (ImageView) findViewById(R.id.arrowtologin);
        arrowLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
            }
        });
    }
}
