package com.example.monline24.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

import com.example.monline24.R;

public class EditCarActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editcar);

        //Panggil Fungsi Button Simpan Edit Mobil
        configurebtnSimpanEditMobil();
    }

    //Fungsi Button Simpan Edit Mobil
    private void configurebtnSimpanEditMobil() {
        Button btnMasuk = (Button) findViewById(R.id.btnSimpanEditMobil);
        btnMasuk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Fungsi Simpan Edit Mobil Berhasil", Toast.LENGTH_LONG).show();
                finish();
            }
        });
    }

}
