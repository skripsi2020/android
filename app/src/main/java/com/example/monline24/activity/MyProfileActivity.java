package com.example.monline24.activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.monline24.R;

import java.io.FileNotFoundException;
import java.io.IOException;

public class MyProfileActivity extends AppCompatActivity {
    int SELECT_PHOTO = 1;
    Uri uri;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);

        //Arrow Back Initialization
        ImageView back = findViewById(R.id.arrowtopengaturan);

        Button Choose = findViewById(R.id.btn_Choose_userPhotoProfile);
        imageView = findViewById(R.id.image);

        //Arrow Back Action on Click
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Choose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, SELECT_PHOTO);
            }
        });

        //Panggil Fungsi Button Ubah Data User
        configurebtnUbahDataUser();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1000) {
                Uri returnUri = data.getData();
                try {
                    Bitmap bitmapImage = MediaStore.Images.Media.getBitmap(MyProfileActivity.this.getContentResolver(), uri);
                    imageView.setImageBitmap(bitmapImage);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
    }

    //Fungsi Button Ubah Data User
    private void configurebtnUbahDataUser() {
        Button btnMasuk = (Button) findViewById(R.id.btnUbahDataUser);
        btnMasuk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Fungsi Ubah Data User Berhasil", Toast.LENGTH_LONG).show();
                finish();
            }
        });
    }

}
