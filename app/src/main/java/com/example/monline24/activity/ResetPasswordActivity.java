package com.example.monline24.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.monline24.R;

//Halaman cadangan kalau dibutuhin

public class ResetPasswordActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        configurearrowtologin();

        configurebtnKonfirmasiResetPassword();
    }

    private void configurebtnKonfirmasiResetPassword() {
        Button btnKonfirmasiResetPassword = (Button) findViewById(R.id.btnKonfirmasiResetPassword);
        btnKonfirmasiResetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ResetPasswordActivity.this, LoginActivity.class));
            }
        });
    }

    private void configurearrowtologin() {
        ImageView arrowLogin = (ImageView) findViewById(R.id.arrowtologin);
        arrowLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ResetPasswordActivity.this, LoginActivity.class));
            }
        });
    }
}
