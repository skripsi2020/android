package com.example.monline24.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.monline24.R;

public class ForgetPassActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_pass);

        //Call Function Panah kembali ke Login
        configurearrowtologin();

        //Call Function Tombol Konfirmasi Lupa Pass
        configureBtnKonfirmasiLupaPass();
    }
    //Fungsi Tombol Konfirmasi di Page Lupa Pass
    private void configureBtnKonfirmasiLupaPass() {
        Button btnKonfirmasiLupaPass = (Button) findViewById(R.id.btnKonfirmasiLupaPass);
        btnKonfirmasiLupaPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ForgetPassActivity.this, LoginActivity.class));
                Toast.makeText(getApplicationContext(), "Cek E-mail Anda Untuk Mendapatkan Link Reset Password", Toast.LENGTH_LONG).show();
            }
        });
    }


    //Fungsi Panah Untuk Kembali Ke Login
    private void configurearrowtologin() {
        ImageView arrowLogin = (ImageView) findViewById(R.id.arrowtologin);
        arrowLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ForgetPassActivity.this, LoginActivity.class));
            }
        });
    }
}
