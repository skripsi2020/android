package com.example.monline24.oauth;

import com.example.monline24.constant.GeneralConstant;
import com.example.monline24.controller.LoginController;
import com.example.monline24.model.request.UserRequest;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import retrofit.RestAdapter;
import retrofit.android.AndroidLog;
import retrofit.client.OkClient;
import retrofit.converter.JacksonConverter;

public class RestClient {

    private static LoginController loginController;

    static {
        setUpRestClient();
    }

    private RestClient() {
    }

    public static LoginController getLogin() {
        return loginController;
    }

    private static void setUpRestClient() {

        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setConnectTimeout(100, TimeUnit.SECONDS);
        okHttpClient.setReadTimeout(100, TimeUnit.SECONDS);
        okHttpClient.setWriteTimeout(100, TimeUnit.SECONDS);

        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES, false);

        //Set Client
        AuthInterceptor requestInterceptor = new AuthInterceptor();
        UserRequest request = new UserRequest();
        request.setClient_id(GeneralConstant.CLIENT_ID);
        request.setClient_secret(GeneralConstant.CLIENT_SECRET);
        requestInterceptor.setClient(request);

        RestAdapter.Builder builder = new RestAdapter.Builder()
                .setRequestInterceptor(requestInterceptor)
                .setEndpoint(GeneralConstant.AUTH_URL)
                .setClient(new OkClient(okHttpClient))
                .setConverter(new JacksonConverter(mapper))
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setLog(new AndroidLog("event"));

        RestAdapter restAdapter = builder.build();
        loginController = restAdapter.create(LoginController.class);
    }
}
