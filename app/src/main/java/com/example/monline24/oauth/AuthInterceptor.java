package com.example.monline24.oauth;

import android.util.Log;

import com.example.monline24.model.request.UserRequest;

import retrofit.RequestInterceptor;

public class AuthInterceptor implements RequestInterceptor {

    private static final String TAG = AuthInterceptor.class.getCanonicalName();

    private UserRequest request;

    @Override
    public void intercept(RequestFacade requestFacade) {
        String authorizationValue = encodeCredentialsForBasicAuthorization();
        requestFacade.addHeader("Authorization", authorizationValue);
    }

    private String encodeCredentialsForBasicAuthorization() {
        String usernameAndPassword = request.getClient_id() + ":" + request.getClient_secret();
        Log.d(TAG, "encodeCredentialsForBasicAuthorization() called with: " + android.util.Base64.encodeToString(usernameAndPassword.getBytes(), android.util.Base64.URL_SAFE));
        return "Basic " + android.util.Base64.encodeToString(usernameAndPassword.getBytes(), android.util.Base64.URL_SAFE);
    }

    void setClient(UserRequest request) {
        this.request = request;
    }
}
